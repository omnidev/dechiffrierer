import UIKit

extension UIImage{
    class func multiply(image:UIImage, color:UIColor) -> UIImage? {
        let rect = CGRect(origin: .zero, size: image.size)
        
        //image colored
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
        color.setFill()
        UIRectFill(rect)
        let coloredImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        //image multiply
        UIGraphicsBeginImageContextWithOptions(image.size, true, 0)
        let context = UIGraphicsGetCurrentContext()
        
        // fill the background with white so that translucent colors get lighter
        context!.setFillColor(UIColor.white.cgColor)
        context!.fill(rect)
        
        image.draw(in: rect, blendMode: .normal, alpha: 1)
        coloredImage?.draw(in: rect, blendMode: .multiply, alpha: 1)
        
        let result = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return result
    }
}

class MainViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var overlayView: OverlayView!
    
    @IBAction func takePhoto(sender: UIBarButtonItem) {
        let vc = UIImagePickerController()
        vc.sourceType = .camera
        vc.allowsEditing = true
        vc.delegate = self
        present(vc, animated: true)
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let image = info[UIImagePickerControllerOriginalImage] as! UIImage
        let pImage = UIImage.multiply(image: image, color: UIColor.red)
        imageView.image = pImage
        
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func logSolution(sender: UIBarButtonItem) {
        let alertController = UIAlertController(title: "Log Solution",
                                                message: "Please enter your solution.",
                                                preferredStyle: .alert)
        alertController.addTextField(configurationHandler: { textField in
            textField.autocorrectionType = .no
        })
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel))
        alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
            guard let solution = alertController.textFields?[0].text else {
                return
            }
            let solutionLogger = SolutionLogger(viewController: self)
            solutionLogger.logSolution(LogEntry(task: "Dechiffrierer", solution: solution))
            // In der Konstante solution steht das Lösungswort, das vom Benutzer eingegeben wurde.
            // TODO: Dieses Wort muss jetzt ähnlich wie beim Metalldetektor im Logbuch eingetragen werden.
        }))
        
        present(alertController, animated: true)
    }
}

