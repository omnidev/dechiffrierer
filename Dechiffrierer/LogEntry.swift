import Foundation

struct LogEntry: Codable {
    let task: String
    let solution: String
}

